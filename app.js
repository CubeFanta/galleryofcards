const slides = document.querySelectorAll('.slide');

for(const slide of slides){
    slide.addEventListener('click', () => {
        clearActiveClasses()


        slide.classList.add('active')
    })
}

function clearActiveClasses() {
    slides.forEach((slide) => {
        slide.classList.remove('active')
    })
};


const item = document.querySelector('.item')

item.addEventListener('dragstart', dragstart)
item.addEventListener('dragend', dragend)

function dragstart(event) {
	event.target.classList.add('hold')
}
function dragend(event) {
	event.target.classList.remove('hold')
}
